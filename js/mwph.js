function mwpht_do() {
var container_height = $('#container').height();
var window_height =$(window).height();
var top_offset = Math.floor((window_height - container_height) / 2);
if (window_height > container_height)
  $('#container').css('margin-top', top_offset);
else
  $('#container').css('margin-top', '2em');
}

$(function() {
  mwpht_do();
  $(window).resize(mwpht_do);
  $('.poll :radio').click(function(e) {
    $(this).parents('form').submit();
  });
});

